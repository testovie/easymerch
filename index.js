const ENEMY_DAMAGE = 5
const ENEMY_HP = 40
const ENEMY_COUNT = 10

const DIFFICULTY = 1.2

const PLAYER_DAMAGE = 10
const PLAYER_HP = 100

const HP_BUFF_VALUE = 30
const HP_COUNT = 10

const SWORD_BUFF_VALUE = 10
const SWORD_COUNT = 2

let is_music_enabled = false
const BG_MUSIC = new Audio('sounds/bgmusic.mp3');
const HIT_SOUND = new Audio('sounds/hit.mp3')

function playMusic(){
    if(is_music_enabled){
        return false
    }
    is_music_enabled = true
    BG_MUSIC.play();
    BG_MUSIC.loop =true
    return true
}
class Game {
    #player
    #enemies = []
    #swords = []
    #hps = []
    #tiles
    #map
    #width
    #height
    #current_difficulty
    #rooms_completed_count
    constructor(container) {
        this.container = container
        this.#width = container.clientWidth/50;
        this.#height = container.clientHeight/50;
        this.#current_difficulty = 1
        this.#rooms_completed_count = 0
    }
    
    init(){
        //заполнение карты пустотой
        this.#clearMap()

        //процедурная генерация карты
        this.#generateMap()

        //Создание персонажа
        this.#initPlayer()
        
        this.#spawnAllEntities()

        setTimeout(()=>{
            this.#initControls()
        },100)
        
    }
    // Рендер текущего кадра игры
    // tiles - содержит статичную сгенерированную карту
    // map - содержит карту с наложенными на нее сущностями
    #render(){
        this.#projectionEntities()
        this.container.innerHTML = ''
        for(let i=0;i<this.#getMapSize().i;i++){
            for(let j=0;j<this.#getMapSize().j;j++){
                let tmp = document.createElement('div')
                tmp.classList.add('tile')
                
                tmp.classList.add(this.#getTile(i,j).type.class)
                if(this.#getTile(i,j).type.class == 'tileP'){
                    let health_block = document.createElement('div')
                    health_block.classList.add('health')
                    health_block.style.width = `${this.#player.getHealth()*100/PLAYER_HP}%`
                    tmp.appendChild(health_block)
                }
                if(this.#getTile(i,j).type.class == 'tileE'){
                    let health_block = document.createElement('div')
                    health_block.classList.add('health')
                    health_block.style.width = `${this.#getTile(i,j).getHealth()*100/(ENEMY_HP*this.#current_difficulty)}%`
                    tmp.appendChild(health_block)
                }
                tmp.style.top = `${i*50}px`
                tmp.style.left = `${j*50}px`
                this.container.appendChild(tmp)
            }
        }
        let body = document.querySelector('.info-wrapper')
        let old_data = document.querySelector('.datainfo')
        if(old_data) old_data.remove()
        let data = document.createElement('div')
        data.classList.add('datainfo')
        data.innerHTML = 
        `
        <h1>${this.#player.getHealth()}/${PLAYER_HP} HP</h1>
        <h1>${this.#player.getDamage()} DMG</h1>
        <h1>Room ${this.#rooms_completed_count+1}</h1>
        <h1>Difficulty: x${this.#current_difficulty.toFixed(2)}</h1>
        `
        body.appendChild(data)
    }
    // Добавление текущего положения сущностей на тайлкарту
    #projectionEntities(){
        this.#initEmptyRender()
        let player_position = this.#player.getPosition()
        this.#addEntity(player_position.x,player_position.y, this.#player)
        for(let i=0;i<this.#enemies.length;i++){
            let position = this.#enemies[i].getPosition()
            this.#addEntity(position.x,position.y, this.#enemies[i])
        }
        for(let i=0;i<this.#swords.length;i++){
            let position = this.#swords[i].getPosition()
            this.#addEntity(position.x,position.y, this.#swords[i])
        }
        for(let i=0;i<this.#hps.length;i++){
            let position = this.#hps[i].getPosition()
            this.#addEntity(position.x,position.y, this.#hps[i])
        }
    }
    // подготавливает пустой рендер(тайлкарта без сущностей)
    #initEmptyRender(){
        let size = this.#getMapSize()
        let height = size.i
        let width = size.j
        this.#map = Array(height).fill(new Empty()).map(() => Array(width).fill(new Empty()));
        for(let i=0;i<height;i++){
            this.#map[i] = this.#tiles[i].slice();
        }
    }
    // добавление сущности на рендер-карту
    #addEntity(x,y,value){
        this.#map[y][x] = value
    }

    // проверка клетки рендер-карты. 
    #checkMove(x,y, is_player=true){
        let tile = this.#getTile(x,y)
        return {
            permitted: is_player?tile.type.can_go_through:tile.type.can_go_enemy, // можно ли наступать
            is_sword_buff: tile instanceof Sword, // является ли баффом меча
            is_hp_buff: tile instanceof Hp, // является ли баффом хп
            entity_id: tile.id // айди тайла на клетке
        }
    }

    // инициализация игрока
    #initPlayer(){
        let player_start_position = this.#getFreePosition()
        this.#player = new Player(PLAYER_HP,PLAYER_DAMAGE, player_start_position.x, player_start_position.y)
        this.#projectionEntities()
    }

    // методы для нахождения индекса в массиве сущностей по айди тайла
    #getEntityIndexById(entity_array, id){
        for(let i=0;i<entity_array.length;i++){
            if(entity_array[i].id == id)
                return i
        }
        return false
    }


    #spawnAllEntities(){
        let difficulty = this.#current_difficulty
        this.#spawnEnemies(ENEMY_COUNT, difficulty)
        this.#spawnHps(HP_COUNT)
        this.#spawnSwords(SWORD_COUNT)
    }
    // три метода спавна сущностей
    #spawnEnemies(ammount, difficulty){
        this.#enemies = []
        for(let i=0;i<ammount;i++){
            let pos = this.#getFreePosition()
            this.#enemies.push(new Enemy(ENEMY_HP*difficulty,ENEMY_DAMAGE,pos.x,pos.y))
            console.log(this.#enemies[i])
            this.#projectionEntities()
        }
    }
    #spawnHps(ammount){
        this.#hps = []
        for(let i=0;i<ammount;i++){
            let pos = this.#getFreePosition()
            this.#hps.push(new Hp(pos.x,pos.y, HP_BUFF_VALUE))
            this.#projectionEntities()
        }
    }
    #spawnSwords(ammount){
        this.#swords = []
        for(let i=0;i<ammount;i++){
            let pos = this.#getFreePosition()
            this.#swords.push(new Sword(pos.x,pos.y, SWORD_BUFF_VALUE))
            this.#projectionEntities()
        }
    }

    // инициализация управления. Недоступно без инициализированного игрока
    #initControls(){
        let game = this
        if(!this.#player){
            console.error('No player specified')
            return -1
        }
        // отдельный евент листенер для предотвращения скролла на спейсбар
        document.addEventListener('keypress',(e)=>{
            if(e.code=='Space')
                e.preventDefault()         
        })
        document.addEventListener('keyup',(e)=>{
            e.preventDefault()
            if(e.ctrlKey || e.shiftKey || e.metaKey || e.altKey || e.key=='Meta')
                return
            if(playMusic()){
                game.#render()
                return
            }
            let move_data = {}
            switch(e.code){
                case 'KeyA':
                    move_data = {
                        x: this.#player.getPosition().x-1,
                        y: this.#player.getPosition().y,
                        orientation: 'left'
                    }
                    break
                case 'KeyD':
                    move_data = {
                        x: this.#player.getPosition().x+1,
                        y: this.#player.getPosition().y,
                        orientation: 'right'
                    }
                    break
                case 'KeyS':
                    move_data = {
                        x: this.#player.getPosition().x,
                        y: this.#player.getPosition().y+1,
                        orientation: 'bottom'
                    }
                    break
                case 'KeyW':
                    move_data = {
                        x: this.#player.getPosition().x,
                        y: this.#player.getPosition().y-1,
                        orientation: 'top'
                    }
                    break
                case 'Space':
                    this.#handleHit()
                    break
                default:
                    return
            }
            
            if(Object.keys(move_data).length!=0){
                let permit_data = this.#checkMove(move_data.y, move_data.x)
                if(!permit_data.permitted){
                    return
                }
                
                this.#player.move(move_data.orientation)
                // если игрок наступает на клетку баффа - применяем бафф
                if(permit_data && permit_data.is_sword_buff){
                    let index = game.#getEntityIndexById(this.#swords, permit_data.entity_id)
                    game.#swords.splice(index,1)
                    game.#player.changeDamage(SWORD_BUFF_VALUE)
                }
                if(permit_data && permit_data.is_hp_buff){
                    let index = game.#getEntityIndexById(this.#hps, permit_data.entity_id)
                    game.#hps.splice(index,1)
                    game.#player.changeHealth(HP_BUFF_VALUE)
                }
            }
            this.#projectionEntities()
            this.#nextMove()
            this.#render()
        })
    }
    // функция удара 
    #handleHit(){
        let hitted_enemies = this.#getEnemiesArround()
        let damage = this.#player.getDamage()
        for(let i=0;i<hitted_enemies.length;i++){
            hitted_enemies[i].changeHealth(-damage)
        }
        HIT_SOUND.play();
    }

    // аналог передачи хода компьютеру - враги бьют, делают ход, а мертвые удаляются
    #nextMove(){
        this.#clearDeadEnemies()
        if(this.#enemies.length==0){
            this.#rooms_completed_count++
            this.#current_difficulty *= DIFFICULTY
            this.#clearMap()
            this.#generateMap()
            this.#spawnAllEntities()
            let new_player_pos = this.#getFreePosition()
            this.#player.setPosition(new_player_pos.x,new_player_pos.y)
            
            this.#render()
        }

        let near_enemies = this.#getEnemiesArround().length
        let damage_taken = near_enemies*ENEMY_DAMAGE
        this.#player.changeHealth(-damage_taken)
        this.#moveEnemies()

        if(this.#player.getHealth()<=0){
            this.#rooms_completed_count = 0
            this.#current_difficulty = 1
            this.#clearMap()
            this.#generateMap()
            this.#initPlayer()
            this.#spawnAllEntities()
        }
    }
    #clearDeadEnemies(){
        for(let i=0;i<this.#enemies.length;i++){
            if(this.#enemies[i].getHealth()<=0){
                this.#enemies.splice(i,1)
            }
        }
    }

    // ход врагов - выбирается случайное направление. если ход возможен - он совершается
    #moveEnemies(){
        let enemies = this.#enemies
        for(let i=0;i<enemies.length;i++){
            let orientations = ['top','left','right','bottom']
            let orientation = orientations[Math.floor(Math.random()*orientations.length)];
            let coords = {}
            switch(orientation){
                case 'top':
                    coords.x = enemies[i].getPosition().x
                    coords.y = enemies[i].getPosition().y-1
                    break
                case 'left':
                    coords.x = enemies[i].getPosition().x-1
                    coords.y = enemies[i].getPosition().y
                    break
                case 'right':
                    coords.x = enemies[i].getPosition().x+1
                    coords.y = enemies[i].getPosition().y
                    break
                case 'bottom':
                    coords.x = enemies[i].getPosition().x
                    coords.y = enemies[i].getPosition().y+1
                    break
            }
            this.#moveEnemy(i,coords.x,coords.y, orientation)
        }
    }
    #moveEnemy(index, x,y, orientation){
        if(this.#checkMove(y, x, false).permitted){
            this.#enemies[index].move(orientation)
        }
        this.#projectionEntities()
    }

    // получаем список врагов в соседних 8 клетках
    #getEnemiesArround(){
        let center_pos = this.#player.getPosition()
        let hitted_enemies = []
        if(this.#getTile(center_pos.y-1,center_pos.x-1) instanceof Enemy)
            hitted_enemies.push(this.#getTile(center_pos.y-1,center_pos.x-1))

        if(this.#getTile(center_pos.y-1,center_pos.x) instanceof Enemy)
            hitted_enemies.push(this.#getTile(center_pos.y-1,center_pos.x))

        if(this.#getTile(center_pos.y-1,center_pos.x+1) instanceof Enemy)
            hitted_enemies.push(this.#getTile(center_pos.y-1,center_pos.x+1))

        if(this.#getTile(center_pos.y,center_pos.x-1) instanceof Enemy)
            hitted_enemies.push(this.#getTile(center_pos.y,center_pos.x-1))

        if(this.#getTile(center_pos.y,center_pos.x+1) instanceof Enemy)
            hitted_enemies.push(this.#getTile(center_pos.y,center_pos.x+1))

        if(this.#getTile(center_pos.y+1,center_pos.x-1) instanceof Enemy)
            hitted_enemies.push(this.#getTile(center_pos.y+1,center_pos.x-1))

        if(this.#getTile(center_pos.y+1,center_pos.x) instanceof Enemy)
            hitted_enemies.push(this.#getTile(center_pos.y+1,center_pos.x))

        if(this.#getTile(center_pos.y+1,center_pos.x+1) instanceof Enemy)
            hitted_enemies.push(this.#getTile(center_pos.y+1,center_pos.x+1))

        return hitted_enemies
    }

    // вспомогательная функция - получение рандомных свободных координат
    #getFreePosition(){
        let x,y
        do {
            x = randomInteger(1, this.#getMapSize().j-1)
            y = randomInteger(1, this.#getMapSize().i-1)
        } while(this.#getTile(y,x).type.class != 'tile')
        return {x,y}
        
    }
    // заполнение массива карты пустотой
    #clearMap(){
        this.#tiles = Array(this.#getMapSize().i).fill(new Empty()).map(() => Array(this.#getMapSize().j).fill(new Empty()));
    }

    // меняет тайл на ТАЙЛ-мапе, не на рендер-мапе
    #changeTile(i,j, value){
        this.#tiles[i][j] = value;
    }

    // получаем тайл на рендер-мапе
    #getTile(i,j){
        return this.#map[i][j];
    }

    #getMapSize(){
        return {
            i: this.#height,
            j: this.#width
        }
    }

    // процедурная генерация карты. Некий аналог алгоритма BSP-дерева
    #generateMap(){
        let rects = []
        let lines = []
        var counter = 0
        let game = this
        addWalls()
        createRooms()
        addPasses()
        this.#initEmptyRender()

        // стены
        function addWalls(){
            for(let i=0;i<game.#getMapSize().i;i++){
                if(i==0 || i==game.#getMapSize().i-1){
                    for(let j=0;j<game.#getMapSize().j;j++){
                        game.#changeTile(i,j, new Wall())
                    }
                }
                else {
                    game.#changeTile(i,0, new Wall())
                    game.#changeTile(i,game.#getMapSize().j-1, new Wall())
                }
            }
        }
        


        // запуск рекурсии - создание комнат
        function createRooms(){
            let rect = new Room({
                start_x: 1,
                start_y: 1,
                end_x: game.#getMapSize().j-1,
                end_y: game.#getMapSize().i-1,
                relation: []
            })
            rects.push(rect)
            generateLine(rect, true)
        }
        

        // Создание проходов в комнатах
        function addPasses(){
            for(let i=0;i<lines.length;i++){
                let entrance_count = randomInteger(3,5)
                let current_count = 0
                while(current_count < entrance_count){
                    let x,y
                    x = randomInteger(lines[i].start_x+1,lines[i].end_x-1)
                    y = randomInteger(lines[i].start_y+1,lines[i].end_y-1)
                    game.#changeTile(y,x,new Empty())
                    current_count++
                }
            }
        }

        // процедурная генерация комнат путем рекурсивного разбиения пространства на две рандомные части
        function generateLine(rect, is_vertical){
            // до 10 комнат
            if(counter>=10) return
            if((rect.end_x-rect.start_x<5) || (rect.end_y-rect.start_y<5)) {
                return
            }
            
            let line_pos

            // создание стены
            if(is_vertical){
                line_pos = randomInteger(rect.start_x+3, rect.end_x-3)
                for(let i=rect.start_y;i<rect.end_y;i++){
                    game.#changeTile(i,line_pos, new Wall())
                }
            }
            else {
                line_pos = randomInteger(rect.start_y+2, rect.end_y-2)
                for(let i=rect.start_x;i<rect.end_x;i++){
                    game.#changeTile(line_pos, i, new Wall())
                }
            }
            // инкремент счетчика
            counter++

            // дробление на комнаты и запоминание линии
            let rect_1 = new Room({
                start_x: rect.start_x,
                start_y: rect.start_y,
                end_x: is_vertical?line_pos:rect.end_x,
                end_y: is_vertical?rect.end_y:line_pos
            })
            let rect_2 = new Room({
                start_x: is_vertical?line_pos:rect.start_x,
                start_y: is_vertical?rect.start_y:line_pos,
                end_x: rect.end_x,
                end_y: rect.end_y
            })
            let line = new Line({
                start_x: is_vertical?line_pos:rect.start_x,
                end_x: is_vertical?line_pos:rect.end_x,
                start_y: is_vertical?rect.start_y:line_pos,
                end_y: is_vertical?rect.end_y:line_pos,
                relation: [rect_1.id, rect_2.id]
            })

            //удаление старого прямоугольника из массива и добавление новых дочерних
            let rect_index =  findRect(rect.id)
            rects.splice(rect_index, 1)
            rects.push(rect_1)
            rects.push(rect_2)

            // нахождение ранее записанной линии по айди старого(не дробленного) прямоугольника
            let old_line_index = findLineByRectId(rect.id)
            let old_line = lines[old_line_index]
            
            // если такая присутствует - тоже дробим ее по точке пересечения новой и старой линии
            if(old_line){
                let new_line1, new_line2

                //пересечение двух линий
                let inter = findIntersection(line.start_x, line.start_y, line.end_x, line.end_y, old_line.start_x, old_line.start_y, old_line.end_x, old_line.end_y)

                if(inter){
                    let is_vertical
                    if(old_line.start_x == old_line.end_x) is_vertical = true
                    else is_vertical = false

                    new_line1 = new Line({
                        start_x: old_line.start_x,
                        end_x: is_vertical?old_line.end_x:inter.x,
                        start_y: old_line.start_y,
                        end_y: is_vertical?inter.y:old_line.end_y,
                        relation: [rect.id, rect_1.id]
                    })
                    new_line2 = new Line({
                        start_x: is_vertical?old_line.start_x:inter.x,
                        end_x: old_line.end_x,
                        start_y: is_vertical?inter.y:old_line.start_y,
                        end_y: old_line.end_y,
                        relation: [rect.id, rect_2.id]
                    })
                    lines.splice(old_line_index, 1)
                    lines.push(new_line1)
                    lines.push(new_line2)
                }
                
            }
            
            
            //добавляем новую линию
            lines.push(line)
            
            //рекурсивно дробим левую и правую части прямоугольника
            generateLine(rect_1, !is_vertical)
            generateLine(rect_2, !is_vertical)
        }
         // поиск прямоугольника по айди
         function findRect(id){
            for(let i=0;i<rects.length;i++){
                if(rects[i].id == id){ 
                    return i;
                }
            }
            return false
        }

        // нахождение линии по айди связанного прямоугольника
        function findLineByRectId(id){
            for(let i=0;i<lines.length;i++){
                if(lines[i].relation.includes(id)){ 
                    return i;
                }
            }
            return false
        }

        // нахождение пересечения двух линий
        function findIntersection(x1, y1, x2, y2, x3, y3, x4, y4){
            const a1 = y2 - y1;
            const b1 = x1 - x2;
            const c1 = a1 * x1 + b1 * y1;
            
            const a2 = y4 - y3;
            const b2 = x3 - x4;
            const c2 = a2 * x3 + b2 * y3;
            
            const determinant = a1 * b2 - a2 * b1;
            if (determinant === 0) {
                return null;
            } else {
                const x = (b2 * c1 - b1 * c2) / determinant;
                const y = (a1 * c2 - a2 * c1) / determinant;
            
                if (x < Math.min(x1, x2) || x > Math.max(x1, x2) ||
                    x < Math.min(x3, x4) || x > Math.max(x3, x4) ||
                    y < Math.min(y1, y2) || y > Math.max(y1, y2) ||
                    y < Math.min(y3, y4) || y > Math.max(y3, y4)) {
                return null;
                } else {
                return { x: x, y: y };
                }
            }
        }        
    }
}

const tileType = {
    BLANK: {
        class: 'tile',
        can_go_through: true,
        can_go_enemy: true,
    },
    ENEMY: {
        class: 'tileE',
        can_go_through: false,
        can_go_enemy: false,
    },
    HP: {
        class: 'tileHP',
        can_go_through: true,
        can_go_enemy: false,
    },
    PLAYER: {
        class: 'tileP',
        can_go_through: false,
        can_go_enemy: false,
    },
    SWORD: {
        class: 'tileSW',
        can_go_through: true,
        can_go_enemy: false,
    },
    WALL: {
        class: 'tileW',
        can_go_through: false,
        can_go_enemy: false,
    },
}





class Tile {
    constructor(type){
        function uuidv4() {
            return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
              (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
          }
        this.type = type
        this.id = uuidv4()
    }
    

}
class Entity extends Tile {
    #hp
    #damage
    #x
    #y

    constructor(type, hp, damage,x,y){
        super(type)
        this.#hp = hp
        this.#damage = damage
        this.#x = x
        this.#y = y
    }
    getHealth(){
        return this.#hp
    }
    changeHealthRaw(value){
        this.#hp = value
    }
    changeDamageRaw(value){
        this.#damage = value
    }
    getDamage(){
        return this.#damage
    }
    
    getPosition(){
        return {
            x: this.#x,
            y: this.#y
        }
    }
    setPosition(x,y){
        this.#x = x
        this.#y = y
    }
    move(orientation){
        switch(orientation){
            case 'top':
                this.#y--
                break
            case 'bottom':
                this.#y++
                break
            case 'left':
                this.#x--
                break
            case 'right':
                this.#x++
                break
        }
    }
    
    
}

class Enemy extends Entity {
    constructor(hp, damage,x,y){  
        super(tileType.ENEMY,hp,damage,x,y);
    }
    changeHealth(value){
        this.changeHealthRaw(Math.max(0, this.getHealth()+value))
    }
}

class Player extends Entity {
    constructor(hp, damage,x,y){
        super(tileType.PLAYER,hp,damage,x,y);
    }
    changeHealth(value){
        this.changeHealthRaw(Math.max(0, Math.min(this.getHealth()+value, PLAYER_HP)))
    }
    changeDamage(value){
        this.changeDamageRaw(Math.max(0, this.getDamage()+value))
    }
}

class Wall extends Tile {
    constructor(){
        super(tileType.WALL)
    }
}

class Empty extends Tile {
    constructor(){
        super(tileType.BLANK)
    }
    
}

class Consumable extends Tile {
    #x
    #y
    #buff_value
    constructor(type, x, y, buff_value){
        super(type)
        this.#x = x
        this.#y = y
        this.#buff_value = buff_value
    }
    getPosition(){
        return {
            x: this.#x,
            y: this.#y
        }
    }
    getBuffValue(){
        return this.#buff_value
    }
}

class Hp extends Consumable{
    constructor(x,y, buff_value){
        super(tileType.HP,x,y, buff_value);
    }
}

class Sword extends Consumable{
    constructor(x,y, buff_value){
        super(tileType.SWORD,x,y, buff_value);
    }
}

class Rect {
    constructor(obj){
        function uuidv4() {
            return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
              (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
          }
        this.id = uuidv4();
        this.start_x = obj.start_x
        this.start_y = obj.start_y
        this.end_x = obj.end_x
        this.end_y = obj.end_y
    }
}
class Room extends Rect {}

class Line extends Rect {
    constructor(...args){
        super(args[0])
        this.relation = args[0].relation
    }
}

function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
}